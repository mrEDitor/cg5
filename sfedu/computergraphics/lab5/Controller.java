package sfedu.computergraphics.lab5;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public static final String BRIEFINGS[] = {
            "Добавьте точки отсекающего выпуклого многоугольника",
            "Добавьте точки отсекаемого многоугольника",
            "Готово!"
    };
    public static final int STATUS_VIEWPORT = 0;
    public static final int STATUS_DRAWING = 1;
    public static final int STATUS_RESULT = 2;

    @FXML
    public Label briefing;
    @FXML
    public Button toPreviousStepButton;
    @FXML
    public Button toNextStepButton;
    @FXML
    public Pane mainPane;

    private ConvexPolygonBuilder viewport;
    private PolygonBuilder drawing;
    private Polygon result;
    private int status;

    public Controller() {
        viewport = new ConvexPolygonBuilder();
        drawing = new PolygonBuilder();
        result = new Polygon();
        result.setStroke(Color.GREEN);
        result.setStrokeWidth(2.0);
        result.setFill(HatchPatternBuilder.build());
        status = STATUS_VIEWPORT;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        briefing.setText(BRIEFINGS[STATUS_VIEWPORT]);
        mainPane.getChildren().addAll(viewport.getActors());
        mainPane.getChildren().addAll(drawing.getActors());
        mainPane.getChildren().add(result);
        mainPane.sceneProperty().addListener(
                (obsScene, oldScene, newScene) -> newScene.getAccelerators().put(
                        new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN), this::cancel
                )
        );
    }

    private void cancel() {
        switch (status) {
            case STATUS_VIEWPORT:
                viewport.cancel();
                break;
            case STATUS_DRAWING:
                drawing.cancel();
                break;
        }
    }

    public void toPreviousStep(ActionEvent event) {
        switch (status) {
            case STATUS_VIEWPORT:
                viewport.clear();
                break;
            case STATUS_DRAWING:
                drawing.clear();
                viewport.unbuild();
                status = STATUS_VIEWPORT;
                briefing.setText(BRIEFINGS[STATUS_VIEWPORT]);
                break;
            case STATUS_RESULT:
                result.getPoints().clear();
                drawing.unbuild();
                status = STATUS_DRAWING;
                briefing.setText(BRIEFINGS[STATUS_DRAWING]);
                toNextStepButton.setDisable(false);
                break;
        }
    }

    public void previewNextStep(MouseEvent mouseEvent) {
        switch (status) {
            case STATUS_VIEWPORT:
                viewport.preview();
                break;
            case STATUS_DRAWING:
                drawing.preview();
                break;
        }
    }

    public void toNextStep(ActionEvent event) {
        switch (status) {
            case STATUS_VIEWPORT:
                if (viewport.isConvex(false)) {
                    viewport.build();
                    status = STATUS_DRAWING;
                    briefing.setText(BRIEFINGS[STATUS_DRAWING]);
                }
                break;
            case STATUS_DRAWING:
                drawing.build();
                final Collection<Point2D> points = Algorithms.clipWithSutherlandHodgman(drawing, viewport);
                for (Point2D point : points) {
                    result.getPoints().addAll(point.getX(), point.getY());
                }
                toNextStepButton.setDisable(true);
                status = STATUS_RESULT;
                briefing.setText(BRIEFINGS[STATUS_RESULT]);
                break;
        }
    }

    public void onMouseClicked(MouseEvent event) {
        final double x = event.getX();
        final double y = event.getY();
        switch (status) {
            case STATUS_VIEWPORT:
                viewport.fixLastPoint(x, y);
                break;
            case STATUS_DRAWING:
                drawing.fixLastPoint(x, y);
                break;
        }
    }

    public void onMouseMoved(MouseEvent event) {
        final double x = event.getX();
        final double y = event.getY();
        switch (status) {
            case STATUS_VIEWPORT:
                viewport.moveLastPoint(x, y);
                break;
            case STATUS_DRAWING:
                drawing.moveLastPoint(x, y);
                break;
        }
    }
}
