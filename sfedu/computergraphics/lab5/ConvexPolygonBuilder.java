package sfedu.computergraphics.lab5;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

/**
 * Convex polygon with unvalidated last point, designed to trace a mouse pointer
 */
public class ConvexPolygonBuilder extends PolygonBuilder {

    protected Stack<Double> angleSignum;

    public ConvexPolygonBuilder() {
        super();
        angleSignum = new Stack<>();
        angleSignum.push(0.0);
    }

    @Override
    public void clear() {
        super.clear();
        angleSignum.clear();
        angleSignum.push(0.0);
    }

    @Override
    public void fixLastPoint(double x, double y) {
        moveLastPoint(x, y);
        if (isConvex(true)) {
            if (shape.getPoints().size() == 2) {
                closingLine.setEndX(x);
                closingLine.setEndY(y);
                angleSignum.push(0.0);
            } else if (shape.getPoints().size() == 4) {
                closingLine.setVisible(true);
                angleSignum.push(0.0);
            } else if (angleSignum.peek() == 0.0) {
                Point2D previousVector = getNthVector(getPointsCount() - 3);
                Point2D activeVector = getNthVector(getPointsCount() - 2);
                angleSignum.push(Math.signum(Algorithms.getAngleBetweenVectors(previousVector, activeVector)));
            } else {
                angleSignum.push(angleSignum.peek());
            }
            shape.getPoints().addAll(x, y);
        }
    }

    @Override
    public void moveLastPoint(double x, double y) {
        super.moveLastPoint(x, y);
        Color paint = isConvex(true) ? Color.BLACK : Color.RED;
        shape.setStroke(paint);
        closingLine.setStroke(paint);
    }

    @Override
    public void cancel() {
        super.cancel();
        int pointsCount = getPointsCount();
        if (pointsCount > 1) {
            angleSignum.pop();
        }
    }

    @Override
    public void preview() {
        super.preview();
        final Color color = isConvex(false) ? Color.GREEN : Color.RED;
        shape.setStroke(color);
        closingLine.setStroke(color);
    }

    @Override
    public void build() {
        Point2D firstPoint = getNthPoint(0);
        moveLastPoint(firstPoint.getX(), firstPoint.getY());
        shape.setStroke(Color.BLUE);
        closingLine.setVisible(false);
    }

    public boolean isConvex(boolean allowIncomplete) {
        final int pointsCount = getPointsCount();
        if (pointsCount == 1) {
            return allowIncomplete;
        }
        Point2D activeVector = getNthVector(pointsCount - 2);
        if (activeVector.distance(0.0, 0.0) == 0.0) {
            return false;
        }
        if (pointsCount <= 3) {
            return allowIncomplete;
        }
        Point2D firstVector = getNthVector(0);
        Point2D previousVector = getNthVector(pointsCount - 3);
        Point2D closingVector = getNthVector(pointsCount - 1);
        double targetAngleSignum = this.angleSignum.peek();
        if (targetAngleSignum == 0.0) {
            return true;
        }
        double firstAngleSignum = Math.signum(Algorithms.getAngleBetweenVectors(closingVector, firstVector));
        double activeAngleSignum = Math.signum(Algorithms.getAngleBetweenVectors(previousVector, activeVector));
        double closingAngleSignum = Math.signum(Algorithms.getAngleBetweenVectors(activeVector, closingVector));
        return (firstAngleSignum == targetAngleSignum || firstAngleSignum == 0.0)
                && (activeAngleSignum == targetAngleSignum || activeAngleSignum == 0.0)
                && (closingAngleSignum == targetAngleSignum || closingAngleSignum == 0.0);
    }

    /**
     * Get signum of inner angles between edges
     *
     * @return +1.0 for CW-drawn polygon and -1.0 for CCW-drawn
     */
    public double getSignum() {
        return angleSignum.peek();
    }
}
