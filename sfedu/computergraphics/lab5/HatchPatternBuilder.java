package sfedu.computergraphics.lab5;

import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;

public class HatchPatternBuilder {

    final static int SIDE_LENGTH = 10;

    public static ImagePattern build() {
        Canvas canvas = new Canvas(SIDE_LENGTH, SIDE_LENGTH);
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.setStroke(Color.DARKGREEN);
        context.strokeLine(SIDE_LENGTH / 2, 0, SIDE_LENGTH, SIDE_LENGTH / 2);
        context.strokeLine(0, SIDE_LENGTH / 2, SIDE_LENGTH / 2, SIDE_LENGTH);
        SnapshotParameters params = new SnapshotParameters();
        params.setFill(Color.TRANSPARENT);
        Image hatch = canvas.snapshot(params, null);
        return new ImagePattern(hatch, SIDE_LENGTH, 0, SIDE_LENGTH, SIDE_LENGTH, false);
    }
}
