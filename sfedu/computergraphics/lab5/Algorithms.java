package sfedu.computergraphics.lab5;

import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Algorithms {

    public static Collection<Point2D> clipWithSutherlandHodgman(PolygonBuilder drawing, ConvexPolygonBuilder viewport) {
        int drawingEdgesCount = drawing.getEdgesCount();
        ArrayList<Point2D> vertices = new ArrayList<>();
        for (int i = 0; i < drawingEdgesCount; i++) {
            vertices.add(drawing.getNthPoint(i));
        }
        /* +1.0 for CW, -1.0 for CCW */
        double viewportIndexOrder = viewport.getSignum();
        int viewportEdgesCount = viewport.getEdgesCount();
        for (int i = 0; i < viewportEdgesCount; i++) {
            Point2D edgeOrigin = viewport.getNthPoint(i);
            Point2D edgeVector = viewport.getNthVector(i);
            Point2D normalVector = new Point2D(-viewportIndexOrder * edgeVector.getY(), viewportIndexOrder * edgeVector.getX()).normalize();
            vertices = clipToHalfPlane(vertices, edgeOrigin, normalVector);
        }
        return vertices;
    }

    private static ArrayList<Point2D> clipToHalfPlane(ArrayList<Point2D> drawing, Point2D edgeOrigin, Point2D normalVector) {
        ArrayList<Point2D> result = new ArrayList<>();
        int edges = drawing.size();
        for (int i = 0; i < edges; i++) {
            Point2D start = drawing.get(i);
            Point2D end = drawing.get((i + 1) % edges);
            Point2D segmentVector = end.subtract(start);
            Point2D edgeToStartVector = start.subtract(edgeOrigin);
            double angle = getAngleBetweenVectors(normalVector, segmentVector);
            double segmentOffset = -findEdgeAndSegmentIntersection(normalVector, edgeToStartVector, segmentVector);
            Point2D intersection = start.add(segmentVector.multiply(segmentOffset));
            if (Math.abs(angle) < 0.5 * Math.PI) {
                if (segmentOffset < 1.0) {
                    if (segmentOffset > 0.0) {
                        result.add(intersection);
                    }
                    result.add(end);
                }
            }
            if (Math.abs(angle) > 0.5 * Math.PI) {
                if (segmentOffset > 0.0) {
                    if (segmentOffset > 1.0) {
                        result.add(end);
                    } else if (segmentOffset > 0.0) {
                        result.add(intersection);
                    }
                }
            }
        }
        return result;
    }

    private static double findEdgeAndSegmentIntersection(Point2D normalVector, Point2D edgeToSegmentVector, Point2D segmentVector) {
        return normalVector.dotProduct(edgeToSegmentVector) / normalVector.dotProduct(segmentVector);
    }

    public static double getAngleBetweenVectors(Point2D a, Point2D b) {
        return Math.atan2(
                a.getX() * b.getY() - a.getY() * b.getX(),
                a.getX() * b.getX() + a.getY() * b.getY()
        );
    }
}
