package sfedu.computergraphics.lab5;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Polygon with unvalidated last point, designed to trace a mouse pointer
 */
public class PolygonBuilder {

    protected Polyline shape;
    protected Line closingLine;

    public PolygonBuilder() {
        shape = new Polyline(0, 0);
        shape.setStroke(Color.BLACK);
        shape.setStrokeWidth(2.0);
        closingLine = new Line();
        closingLine.setVisible(false);
        closingLine.setStroke(Color.BLACK);
        closingLine.getStrokeDashArray().addAll(5.0, 10.0);
        closingLine.setStrokeWidth(2.0);
    }

    public Collection<Shape> getActors() {
        ArrayList<Shape> actors = new ArrayList<>();
        actors.add(shape);
        actors.add(closingLine);
        return actors;
    }

    public void clear() {
        shape.getPoints().setAll(0.0, 0.0);
        closingLine.setVisible(false);
    }

    public void fixLastPoint(double x, double y) {
        moveLastPoint(x, y);
        if (shape.getPoints().size() == 2) {
            closingLine.setEndX(x);
            closingLine.setEndY(y);
        } else if (shape.getPoints().size() == 4) {
            closingLine.setVisible(true);
        }
        shape.getPoints().addAll(x, y);
    }

    public void moveLastPoint(double x, double y) {
        final int coordinates = shape.getPoints().size();
        shape.getPoints().set(coordinates - 2, x);
        shape.getPoints().set(coordinates - 1, y);
        closingLine.setStartX(x);
        closingLine.setStartY(y);
    }

    public void cancel() {
        int pointsCount = getPointsCount();
        if (pointsCount > 1) {
            shape.getPoints().remove(2 * pointsCount - 2, 2 * pointsCount);
            Point2D lastPoint = getNthPoint(pointsCount - 2);
            moveLastPoint(lastPoint.getX(), lastPoint.getY());
            closingLine.setVisible(shape.getPoints().size() != 2);
        }
    }

    public void preview() {
        Point2D firstPoint = getNthPoint(0);
        moveLastPoint(firstPoint.getX(), firstPoint.getY());
    }

    public void build() {
        Point2D firstPoint = getNthPoint(0);
        moveLastPoint(firstPoint.getX(), firstPoint.getY());
        shape.setStroke(Color.MAGENTA);
        shape.getStrokeDashArray().addAll(5.0, 10.0);
        closingLine.setVisible(false);
    }

    public void unbuild() {
        closingLine.setVisible(true);
        shape.setStroke(Color.BLACK);
        shape.getStrokeDashArray().clear();
    }

    public Point2D getNthPoint(int n) {
        int upperBound = shape.getPoints().size();
        Double x = shape.getPoints().get((2 * n) % upperBound);
        Double y = shape.getPoints().get((2 * n + 1) % upperBound);
        return new Point2D(x, y);
    }

    public int getPointsCount() {
        return shape.getPoints().size() / 2;
    }

    public int getEdgesCount() {
        return shape.getPoints().size() / 2 - 1;
    }

    public Point2D getNthVector(int n) {
        Point2D firstPoint = getNthPoint(n);
        Point2D secondPoint = getNthPoint(n + 1);
        return secondPoint.subtract(firstPoint);
    }
}
